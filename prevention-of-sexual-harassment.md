### What kinds of behaviour cause sexual harassment?

Any kind of **verbal**, **visual** or **physical** behaviour that is inappropriate, offensive or causes someone to be uncomfortable are causes of sexual harassment. These can include:
- Inappropriate comments towards or in presence of someone (verbal)
- Sexual or gendered based jokes (verbal)
- Threats or spreading rumors (verbal)
- Showcasing inappropriate posters, pictures, texts or cartoons (visual)
- Inappropriate touching or staring  or invasion of personal space (physical)

-----
-----

### What would you do in case you face or witness any incident or repeated incidents of such behaviour?

When faced with any such incidents, I would:
- Tell the person(wrongdoer) to stop
- Get help from my employer
- Follow the grievance procedure as specified by the organization
- Report it to HR or someone higher up that I am comfortable sharing it with
