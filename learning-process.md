# Learning Process

## Question 1
What is the Feynman Technique? Explain in 1 line.

### Answer 1
The Feynman technique is a studying technique where one learns a concept by trying to explain it in simple terms, as simple as explaining to a five year old.

-----

## Question 2
In this video, what was the most interesting story or idea for you?

### Answer 2
I found Salvador Dali's story most interesting. He seems like an eccentric person. He twirls his keys in his hands when relaxing, and when the sound of the keys falling jolts him awake, he gets into a focused state.

-----

## Question 3
What are active and diffused modes of thinking?

### Answer 3
Active mode of thinking is when one is giving 100% of their attention to the task they are working at the moment.
Diffused mode of thinking is when one is subconsciously thinking about the task when they are relaxing or doing something fun.

-----

## Question 4
According to the video, what are the steps to take when approaching a new topic? Only mention the points.

### Answer 4
1. Simplify the skill into multiple smaller tasks
42. At first, learn just enough to notice when you are making mistakes and self-correct
42. Remove any and all distractions that make you procastinate
42. Commit to give at least 20 full focused hours to learning the skill

-----

## Question 5
What are some of the actions you can take going forward to improve your learning process?

### Answer 5
- Learn topics in a way that makes me capable of explaining it to others, preferably using simple terms
- Approach topics with curiosity. Always ask questions to myself like "Why this concept is used in this specific way, and not another?", "How is this implemented?", "When and where can this be applied?"
- Communicate often and be specific and clear
- Avoid distractions
