# Firewalls

A firewall is a piece of software or hardware that regulates the flow of network traffic between different computers. When receiving or sending data through the internet, the firewall filters potentially harmful data based on its security level configuration. Firewalls maintain the barrier between trusted networks (home network) and untrusted networks (internet).

## The working of a Firewall

A firewall has several different mechanisms to protect the network. Some of them are as follows:

### Packet Filtering
- The firewall passes or blocks data packets at a network interface based on source and destination addresses, ports, or protocols. 
- Packet filtering is the most common and simplest type of firewall.
- Almost all firewall products support packet filtering.

### Stateful Inspection
- The firewall maintains a secure list of trusted websites that are visited and keeps track of the connection status.
- Any unwanted data sent back by third parties is blocked because the firewall detects that it is not from the actual website.
- Stateful firewalls are more secure than packet filtering firewalls.

### Proxy Firewall
- A proxy firewall adds an extra layer of protection in sending and receiving network traffic.
- It provides an intermediate gateway to which the user's data is sent.
- It encrypts the data being sent to the server so that any third party involved cannot track where the original request came from.

## Limitations of Firewall
- **No verification of incomming data**: Firewalls don't protect against fake data packets coming from a trusted source since they cannot verify the actual packet, only the source.
- **Insider's intrusion**: Firewalls focus on external threats, so they may not prevent insider threats from within the home network.
- **No protection against masquerades**: Firewalls can't protect against compromised credentials used to access systems posing as the user.
- **Not an anti-virus**: Firewalls don't provide any anti-malware or antivirus functions. It is up to the user to deal with viruses or malware that have made it into the system.

-----
-----
## References
- [Firewall - Software and Hardware Explained | Network Security | TechTerms](https://www.youtube.com/watch?v=eO6QKDL3p1I&list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6)
- [What is a Firewall? - Cisco](https://www.cisco.com/c/en_in/products/security/firewalls/what-is-a-firewall.html)
- [What is packet filtering? - TechTarget Definition](https://www.techtarget.com/searchnetworking/definition/packet-filtering)
- [Stateful Inspection -an overview | ScienceDirect Topics](https://www.sciencedirect.com/topics/computer-science/stateful-inspection)
- [What is a Proxy Firewall and How Does it Work? | Fortinet](https://www.fortinet.com/resources/cyberglossary/proxy-firewall#:~:text=A%20proxy%20firewall%2C%20also%20known,or%20inspect%20application%20protocol%20traffic.)
