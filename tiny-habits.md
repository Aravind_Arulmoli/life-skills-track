### Question 1
In this video, what was the most interesting story or idea for you?

#### Answer 1
The idea I found most interesting is that consistently doing things that are easy to do can eventually motivate us to do harder things.

-----
### Question 2
How can you use B = MAP to make making new habits easier? What are M, A and P.

#### Answer 2
When our **motivation** matches our **ability** and there is a **prompt** to trigger our action, we can form new tiny habits (**behavior**). So, **B**ehavior = **M**otivation + **A**bility + **P**rompt.

-----
### Question 3
Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)

#### Answer 4
It is important to "Shine" or Celebrate after each successful completion of habit because it gives us an "authentic pride" and provides us with the motivation to keep continuing the habit. There is "success momentum" created that puts us in a positive feedback loop to grow the habit.

-----
### Question 4
In this video, what was the most interesting story or idea for you?

#### Ansewr 4
I found the story of Dave Brailsford and the British cycling team the most interseting. They went from not winning anything for almost 100 years to dominating Tour de France and the London 2012 Olympics, just by making simple 1% improvements gradually.

-----
### Question 5
What is the book's perspective about Identity?

#### Answer 5
The book's perspective about identy is that "identity change is the north star of habit change". To solve problems in the longer term, we need to change our identity away from solving them for outcomes and results.

-----
### Question 6
Write about the book's perspective on how to make a habit easier to do?

#### Answer 6
The books says that to make a habit easier to do, we should make it obvious, make it attractive and make it immediately satisfying.

-----
### Question 7
Write about the book's perspective on how to make a habit harder to do?

#### Answer 7
To make a habit harder to do, we should make it unattractive, unsatisfying and make cues for it invisible.

-----
### Question 8
Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

#### Answer 8
A habit I would like to do more of would be exercising.
- The cue would be washing my face in the morining.
- Since that completely wakes me up and I am not dressed for work yet, it is easier(attractive) to do stretches and a couple of squats and pushups.
- The shower right after would be satisfying.

-----
### Question 9:
Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

#### Answer 9
I would like to eliminate the habit of drinking coffee often. The steps that I can take for this are:
- I should make the cue less obvious by thinking about things other than drinking coffee.
- Not staying in the cafeteria would require me to make a long walk to the coffee machine that makes it unattractive.
