### Question 1
What is Deep Work?

#### Answer 1
Deep work is the **hard focus** where one gives their complete attention to a cognitively demanding task for a set amount of time.

### Question 2
According to author how to do deep work properly, in a few points?

#### Answer 2
- Schedule distractions by building boundaries on the distractions
- Develop a rhythmic deep work ritual by transfroming the work into a simple regular habit
- Incorporate an eveining shutdown ritual in the daily routine by making a plan for the next day

### Question 3
How can you implement the principles in your day to day life?

#### Answer 3
- Try to put in at least 4 hours of full focused work everyday
- Make a schedule of when I will focus and when I will relax and follow it everyday
- Make of list of tasks I want to complete and review it everyday

### Question 4
What are the dangers of social media, in brief?

#### Answer 4
- Reduced concentration: Social media is designed to be addictive. It encourages constant checking to keep up to date which causes constant distraction and reduces concentration.
- Psychological issues: Higher smartphone usage directely correlates to higher anxiety disorders.
- Impact on professional success: Even though having a large social media following might look like it would lead to better opportunities, building genuine and valuable skills are more important that can be hindered by social media usage.
