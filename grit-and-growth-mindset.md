### Question 1
Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

#### Answer 1
Being successful in life is less about being talented, having good looks or having good social intelligence and more about being **passionate** and having **perseverance**, which is called grit. A strong growth mindset helps in building grit. Growth mindset is the mindset of having the will to accept failures, to be wrong and to learn from those lessons to get better.

-----
### Question 2
Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

#### Answer 2
Growth mindset is where individuals believe that their skills can be improved with effort, whereas fixed mindset is where individuals believe skills are inherently set since some people are naturally good at things while others are not. The type of mindset affects the way the individual learns, i.e, people with growth mindset continue to learn from their mistakes and keep getting better, while people with fixed mindset will only put effort into not failing.

-----
### Question 3
What is the Internal Locus of Control? What is the key point in the video?

#### Answer 3
Internal Locus of Control is when an individual believes that they have the control over the things that happen in their life.
- The amount of work we put is something we have complete over
- Having an internal locus of control helps in staying motivated
- Instead of blaming external sources for our failure, we should take matter into our own hands

-----
### Question 4
What are the key points mentioned by speaker to build growth mindset (explanation not needed).

#### Answer 4
- Believe in your ability to figure things out
- Question yourself about your failures
- Continually learn and grow
- Struggle is a part of the learning process
- Stay calm in the face of challenges

-----
### Question 5
What are your ideas to take action and build Growth Mindset?

#### Answer 5
My ideas to build growth mindset are:
- Believe in my ability to learn and improved
- Question myself whenever I face failures and setbacks
- Don't let those failures and setbacks keep me down and learn from them
- Develop a persevering mindset and be passionate about the things I do
