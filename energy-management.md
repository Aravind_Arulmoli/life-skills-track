Question 1:
What are the activities you do that make you relax - Calm quadrant?

#### Answer 1
- Playing video games
- Laying down and drifting in my own thoughts
- Taking a shower
- Sleeping

### Question 2:
When do you find getting into the Stress quadrant?

#### Answer 2
- Socializing
- Planning things out
- Thinking about the deadline for the next task
- Long extended amuonts of work

### Question 3:
How do you understand if you are in the Excitement quadrant?

#### Answer 3
- Finishing a complex task
- Realizing I have learnt a hard-to-learn skill

### Question 4
Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

#### Answer 4
- Lack of sleep affects reproductive health negatively
- Sleeping well results in better learning activity and better memory
- Lack of proper sleep is often a major cause for people suffering with dementia
- Sleeplessness also impacts the DNA and genetic activity that might be a cause for cancer or reduce immune system functions
- Sleep is fundamental part of living beings, since all creatures evolved to support sleep and not eliminate it

### Question 5
What are some ideas that you can implement to sleep better?

#### Answer 5
- Maintain a consistent sleep schedule.
- Maintain a cooler sleeping temperature around 18°C.
- Avoid taking naps at random times in the day so as to avoid sleeplessness at night.

### Question 6
Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

#### Answer 6
Exercise has the following benefits:
- Immediate positive affects for the brain including better mood, better reaction time and better focus
- These positive affects last for a long time and protect the brain from depression, Alzheimer's and dementia
- Regular exercise changes the brain's anatomy, physiology and function by producing new brain cells increasing the volume of the hippocampus
- The prefontal cortex and the hippocampus get more protected the more exercise you do
- Exercising 3 to 4 times a week with minimum 30 minutes an exercise session is enough to attain all these benefits

### Question 7
What are some steps you can take to exercise more?

#### Answer 7
- Dedicate a certain amount of time (15 - 30 minutes) per day for exercising
- Start with minimal effort so as to build the habit
- Be consistent
- Walk more and use the stairs more
