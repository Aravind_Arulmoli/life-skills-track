### Question 1
What are the steps/strategies to do Active Listening? (Minimum 6 points)

#### Answer 1
1. Avoid getting distracted
42. Focus on the what's being said
42. Don't interrupt the speaker
42. Only respond after the speaker is done speaking
42. Show that you're interested in whatever the speaker is taking about, either by using verbal phrases like "That's interesting" or by using body language.
42. Take notes, only if it is appropriate
42. Paraphrase what the speaker just said so as to show that you have understood and make sure both of you are on the same page.

-----
### Question 2
According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

#### Answer 2
- Understand the speaker in an empathetic manner
- Repeat what the speaker just conveyed in a short way to show that you have really understood them
- Use body language like nodding your head and maintaining eye contact
- Be less critical and don't judge the speaker so that the speaker can be comfortable
- Encourage the speaker to be more open by asking questions that lead to a deeper conversation

-----
### Question 3
What are the obstacles in your listening process?

#### Answer 4
- Empathizing with the speaker if their life experiences are wildly different from mine.
- Occasionally getting distracted by my own thoughts

-----
### Question 4
What can you do to improve your listening?

#### Answer 4
- Maintain eye contact with the speaker
- Actively take notes if appropriate
- Paraphrase what was just said by the speaker
- Pay full attention to the speaker

-----
### Question 5
When do you switch to Passive communication style in your day to day life?

#### Answer 5
When talking to colleagues and non-close friends/acquaintances

-----
### Question 6
When do you switch into Aggressive communication styles in your day to day life?

#### Answer 6
Almost never. The only time I can think of is when I am talking to a customer service agent that's trying to make excuses.

-----
### Question 7
When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

#### Answer 7
When talking to my roommates when they are being annoying or not being responsible for something they should be.

-----
### Question 8
How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

#### Answer 8
- Recognize and name your feelings
- Look for the need behind the need
- Start with easy situations and easy stakes
- Be aware of yourself
- Don't wait to speak up untill all your problems start piling up
