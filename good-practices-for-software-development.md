### Question 1
Which point(s) were new to you?

#### Answer 1
- Point 6.3 : I wasn't aware that preserving attention could be practiced

-----
### Question 2
Which area do you think you need to improve on? What are your ideas to make progress in that area?

#### Answer 2
I have to improve on communication with teammates. Some ideas that can help me progress in this area are:
- Be clear about whatever I'm asking, instead of being vague
- Make time to get to know my teammates better
- Try to be always be available when someone messages or calls me or if I'm not available, inform them when they can contact me next
